# Keyboard layout for temporary Hungarian characters for macOS

Primarily, I write code and use English. Sometimes, I want to write in Hungarian with accents, 
but I don't want to switch keyboard layouts just for a few chars, like `Köszi` (`Thanks` in hungarian).
The `Alt` key comes in handy: while pressing it, I can type charaters with accents (like á,é,...) without switching layouts.
The default bindings are from the American/English international layout (when not pressing `Alt`).

Additional bindings:
```
    Alt+; => é
    Alt+' => á
    Alt+[ => ő
    Alt+] => ú
    Alt+\ => ű
    Alt+0 => ö
    Alt+- => ü
    Alt+= => ó
    Alt+` => í
```

Tested on `macOS Sonoma 14.3.1 (23D60)`.

## Install
- Place the `keybtemphun.keylayout` in `~/Library/Keyboard Layouts`
- System Preferences
    - Keyboard
    - Text Input
    - Edit
    - Press `+` in the lower left to add a new input source
    - Select `Others` from the list on the left
    - Select this keyboard and add
- Make sure you switch to this keyboard ;)

## Uninstall
- Reverse the install process

### Todos
- Icon (icns) for the layout

### Sources
- https://altgr-weur.eu/macos.html
- https://github.com/tjsousa/altgr-weur-mac
- https://github.com/zaki/mac-hun-keyboard


@parhuzamos
